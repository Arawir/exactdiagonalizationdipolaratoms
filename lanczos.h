#ifndef LANCZOS_H
#define LANCZOS_H

#include "commonMethods.h"

inline void orthogonalizeVectors(const int &i, gsl_vector * v0, gsl_vector * v1, const gsl_matrix *V){
    double alf;
    for(int j=0;j<i;j++){
        gsl_matrix_get_col(v0,V,j);
        gsl_blas_ddot(v1,v0,&alf);
        gsl_vector_scale(v0,alf);
        gsl_vector_sub(v1,v0);
    }
}

void lanczos(int neig, int dim, int m, const gsl_spmatrix * H, gsl_matrix * VEC, gsl_vector *VAL){
    std::vector<double> alpha(m,0.0), beta(m,0.0);

    gsl_matrix *V = gsl_matrix_alloc(dim, m);
    printProgress("  LANCZOS: memory allocation ", 0,3);

    gsl_matrix *T = gsl_matrix_alloc(m, m);
    gsl_matrix_set_zero (T);
    printProgress("  LANCZOS: memory allocation ", 1,3);

    gsl_vector * v0 = gsl_vector_alloc (dim);
    gsl_vector_set_zero(v0);
    printDone("  LANCZOS: memory allocation ", 2);


    int maxi=0;
    for(int i=1;i<dim;i++){
        if(gsl_spmatrix_get(H, i, i)>gsl_spmatrix_get(H, maxi, maxi)){
            maxi=i;
        }
    }


    gsl_vector_set(v0,maxi,1);
    gsl_vector * v1 = gsl_vector_alloc (dim);
    gsl_matrix_set_col(V,0,v0);


    for(int i=1;i<m;i++){
        alpha[i-1] = inner(v0,H,v1);

        orthogonalizeVectors(i,v0,v1,V);
        orthogonalizeVectors(i,v0,v1,V); //po pierwotnej ortogonalizacji wektory wcale nie są prostopadłe, błąd numeryki

        beta[i]=gsl_blas_dnrm2(v1);
        gsl_vector_scale(v1,1./beta[i]);
        gsl_vector_memcpy(v0,v1);
        gsl_matrix_set_col(V,i,v0);

        printProgress("  LANCZOS: generate matrix V: ",i,m );
    }

    alpha[m-1] = inner(v0,H,v1);
    gsl_vector_free(v1);

    gsl_matrix_set(T,0,0,alpha[0]);
    for(int i=1;i<m;i++){
        gsl_matrix_set(T,i,i,alpha[i]);
        gsl_matrix_set(T,i,i-1,beta[i]);
        gsl_matrix_set(T,i-1,i,beta[i]);
    }

    gsl_matrix *evec = gsl_matrix_alloc (m, m);
    gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (m);
    gsl_eigen_symmv (T, VAL, evec, w);
    gsl_eigen_symmv_free (w);
    gsl_matrix_free(T);
    gsl_eigen_symmv_sort (VAL, evec, GSL_EIGEN_SORT_ABS_ASC);


    gsl_vector * read = gsl_vector_alloc (m);
    for(int i=m-neig;i<m;i++){
        gsl_matrix_get_col(read,evec,i);
        gsl_blas_dgemv(CblasNoTrans,1.,V,read,0.,v0);
        gsl_matrix_set_col(VEC,i-(m-neig),v0);
    }

    gsl_matrix_free(evec);
    gsl_matrix_free(V);
    gsl_vector_free(read);
    gsl_vector_free(v0);
}


//liczy wariancje energii na wyznaczonych wektorach (na stanach własnych powinna być 0)
std::vector<double> var(int neig, int dim, const gsl_matrix * VEC, const gsl_spmatrix * H){
    std::vector<double> varlist(neig,0.0);
    gsl_vector * v0 = gsl_vector_alloc (dim);
    gsl_vector * v1 = gsl_vector_alloc (dim);
    double xx, x;
    for(int i=0;i<neig;i++){
        gsl_matrix_get_col(v0,VEC,i);
        gsl_spblas_dgemv(CblasNoTrans,0.5,H,v0,0.,v1);
        gsl_spblas_dgemv(CblasTrans,0.5,H,v0,1.,v1);
        xx=gsl_blas_dnrm2(v1);
        gsl_blas_ddot(v1,v0,&x);
        varlist[i]=xx*xx-x*x;
    }
    gsl_vector_free(v0);
    gsl_vector_free(v1);
    return varlist;
}

#endif //LANCZOS_H
